package br.com.grupofleury;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControleAcessoExternoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControleAcessoExternoApplication.class, args);
	}
}
